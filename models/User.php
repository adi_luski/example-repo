<?php
namespace app\models;

use Yii;
use \yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
	public $role; 
	public static function tableName()
    {
        return 'user';
    }
    public function rules()
    {
        return [
            [['username', 'password', 'auth_key'], 'string', 'max' => 255],
			[['username', 'password'], 'required'],
            [['username'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
			'auth_key' => 'auth_key',
        ];
    }	 
	 
	 
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

	public static function findByUsername($username)
	{
		return static::findOne(['username' => $username]);
	}

 /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
		throw new NotSupportedException('You can only login
							by username/password pair for now.');
    }

 
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
         return $this->getAuthKey() === $authKey;
    }

    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

        if ($this->isNewRecord)
		    $this->auth_key = Yii::$app->security->generateRandomString(32);

        return $return;
    }
	public function validatePassword($password)
	{
		return Yii::$app->security->validatePassword($password, $this->password);
	}

    public function getLeads()
	{
		return $this->hasMany(Lead::className(), ['ownerId' => 'id']);
	}
	

 
	

	
	
	
	
}